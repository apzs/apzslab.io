let imageSet = new Set()
setInterval(()=>{
    const imgs = document.getElementsByTagName("img");
    Array.prototype.forEach.call(imgs, function (el) {
    if(el.src && el.src.length > 0 && !imageSet.has(el.src)){
        imageSet.add(el.src)
        console.log("el2:", el);
        const imgSrc = el.src
        el.src = ''
        // 观察者(元素被加载、出现在了可视区域、离开了可视区域 会被触发)
        const observer = new IntersectionObserver((entries) => {
            // console.log("触发了", entries);
            const [{ isIntersecting }] = entries
            if (isIntersecting) {
                // console.log("进入可视区域");
                // 进入可视区域后加载图片
                el.src = imgSrc
                // 取消观察，防止加载完图片后还触发，再次加载图片
                observer.unobserve(el)
            }
        })
        observer.observe(el)
    }
    });
},1000)


// window.onload = ()=>{

// }


// const imgs = document.getElementsByTagName("img");
// console.log('111,直接执行，图片个数',imgs.length);
// console.log('此时还未加载body')
// debugger
// Array.prototype.forEach.call(imgs, function (el) {
// console.log("el2:", el);
// const imgSrc = el.src
// el.src = ''
// // 观察者(元素被加载、出现在了可视区域、离开了可视区域 会被触发)
// const observer = new IntersectionObserver((entries) => {
//     console.log("触发了", entries);
//     const [{ isIntersecting }] = entries
//     if (isIntersecting) {
//         console.log("进入可视区域");
//         // 进入可视区域后加载图片
//         el.src = imgSrc
//         // 取消观察，防止加载完图片后还触发，再次加载图片
//         observer.unobserve(el)
//     }
// })
// observer.observe(el)
// });



// document.addEventListener("DOMContentLoaded", ()=>{
//     const imgs = document.getElementsByTagName("img");
//     console.log('222，html、css加载完成，图片还未加载，图片个数',imgs.length);
//     debugger
//     Array.prototype.forEach.call(imgs, function (el) {
//     console.log("el2:", el);
//     const imgSrc = el.src
//     el.src = ''
//     // 观察者(元素被加载、出现在了可视区域、离开了可视区域 会被触发)
//     const observer = new IntersectionObserver((entries) => {
//         console.log("触发了", entries);
//         const [{ isIntersecting }] = entries
//         if (isIntersecting) {
//             console.log("进入可视区域");
//             // 进入可视区域后加载图片
//             el.src = imgSrc
//             // 取消观察，防止加载完图片后还触发，再次加载图片
//             observer.unobserve(el)
//         }
//     })
//     observer.observe(el)
//     });
// });


// window.onpopstate = function() {
//     const imgs = document.getElementsByTagName("img");
//     console.log('333,url改变时执行,图片个数',imgs.length);
//     debugger
//     Array.prototype.forEach.call(imgs, function (el) {
//     console.log("el2:", el);
//     const imgSrc = el.src
//     el.src = ''
//     // 观察者(元素被加载、出现在了可视区域、离开了可视区域 会被触发)
//     const observer = new IntersectionObserver((entries) => {
//         console.log("触发了", entries);
//         const [{ isIntersecting }] = entries
//         if (isIntersecting) {
//             console.log("进入可视区域");
//             // 进入可视区域后加载图片
//             el.src = imgSrc
//             // 取消观察，防止加载完图片后还触发，再次加载图片
//             observer.unobserve(el)
//         }
//     })
//     observer.observe(el)
//     });
//   };

